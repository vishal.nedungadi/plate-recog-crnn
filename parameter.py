CHAR_VECTOR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "

letters = [letter for letter in CHAR_VECTOR]

num_classes = len(letters)

img_w, img_h = 128, 64

# Network parameters
batch_size = 8
val_batch_size = 4

downsample_factor = 4
max_text_len = 12
